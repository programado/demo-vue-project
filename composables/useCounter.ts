export default function () {
    const count = ref(0)

    return {
        count,
        increment: (stepSize = 1) => {
            count.value += stepSize
        },
        decrement: (stepSize = 1) => {
            count.value -= stepSize
        }
    }
}
