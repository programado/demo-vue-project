# Shop Rahmenbedingungen

## Must Haves:
* Artikel können per UI angelegt werden
* Artikel können eingesehen und gefiltert werden
* Artikel können in den Warenkorb gelegt werden
* Der Warenkorb kann gekauft werden (Pseudokauf)
* Bestellungen können eingesehen werden
* Es gibt Kategorien für Artikel
* Ein Artikel kann einer Kategorie zugeordnet werden
* Ein Artikel kann wenigstens ein Bild speichern

## Nice-To-Haves:
* Gutscheine können definiert und angewandt werden
* Für das Shop-Backend gibt es einen Admin Login
* Kategorien können Ober und Unterkategorien haben (Kategoriebaum)
* Man kann mit der PayPal Sandbox einen Kauf durchführen
* Artikel können unterschiedliche Attribute haben, welche definiert werden können (Bspw. Farbe, Größe, Festplattenkapazität usw.)
 * Kann per EAV-Pattern oder JSON Spalte in der Datenbank umgesetzt werden
